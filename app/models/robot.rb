class Robot
   # directions
   NORTH = 'NORTH'
   SOUTH = 'SOUTH'
   EAST = 'EAST'
   WEST = 'WEST'
 
   # commands
   PLACE = 'PLACE'
   MOVE = 'MOVE'
   LEFT = 'LEFT'
   RIGHT = 'RIGHT'
 
   REPORT = 'REPORT'
 
   DIRECTION_MAPPER = { "NORTH_LEFT": 'WEST', "NORTH_RIGHT": 'EAST',
                        "SOUTH_LEFT": 'EAST', "SOUTH_RIGHT": 'WEST',
                        "EAST_LEFT": 'NORTH', "EAST_RIGHT": 'SOUTH',
                        "WEST_LEFT": 'SOUTH', "WEST_RIGHT": 'NORTH' }.freeze
  
  attr_accessor :x, :y, :orientation, :result

  def initialize
    @x = 0
    @y = 0
    @orientation ="NORTH"
    @result = []
  end

  def execute(commands)
     commands.each do |command|
        if command[0..4] == "PLACE"
            place_robot(command)
        elsif command == "MOVE"
            move_robot
        elsif [LEFT,RIGHT].include?command
            rotate_robot(command)
        elsif command == "REPORT"
            @result.push(@x, @y, @orientation)
        end
     end
  end

  def place_robot(command)
    cords = command.split(" ")[1].split(',')
    @x = cords[0].to_i
    @y = cords[1].to_i
    @orientation = cords[2]
  end
  def move_robot
    if @orientation == "NORTH"
        @y += 1
    elsif @orientation == "SOUTH"
        @y -= 1
    elsif @orientation == "EAST"
        @x += 1
    elsif @orientation == 'WEST'
        @x -= 1
    end
  end
  def valid_move?
    (0...5) === @x && (0...5) === @y
  end
  def rotate_robot(command)
    @orientation = DIRECTION_MAPPER["#{@orientation}_#{command}".to_sym]
  end
end
    