class Api::RobotsController < ApplicationController
    def orders
      commands = params[:commands]
      r = Robot.new
      r.execute(commands)
      render json: {location: r.result}, status: :ok
    end
end
